#!/usr/bin/env python
# coding: utf-8

# # Object Detection Demo
# Welcome to the object detection inference walkthrough!  This notebook will walk you step by step through the process of using a pre-trained model to detect objects in an image. Make sure to follow the [installation instructions](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md) before you start.

# # Imports

# In[12]:


import numpy as np
import os
import tensorflow as tf
from PIL import Image
import time
from flask import Flask
from flask import request
from flask import jsonify
import json
from yolo import YOLO
EXCUTION_PATH = '/app'

yolo = YOLO()
graph = tf.get_default_graph()

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

def image_to_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)


def predict_image(image):
    image = Image.open(image)
    image = image.convert("RGB")
    with graph.as_default():
        results = yolo.detect_image(image)
         import cv2
         images = []
         for result in results:
             images = result['image']
         cv2.imwrite("out.jpg", np.asarray(images)[..., ::-1])
        return results

def return_format(data):
    results = []
    for result in data:
        box = result['box_points']

        raw_result = {}
        raw_result['box_points'] = [box[1],box[0],box[3],box[2]]
        raw_result['percentage_probability'] = result['percentage_probability'] * 100
        raw_result['name'] = result['name']

        results.append(raw_result)
    print(results)
    return results

app = Flask(__name__)




@app.route("/")
def home():
    return "Welcome to keras-yolo3 Object Detection API!"

@app.route("/predict", methods=['POST'])
def predict():
    if request.method == 'POST':
        data = request.files
        try:
            data = predict_image(data['image'])
            data = return_format(data)
            data = json.dumps(data, cls=MyEncoder)
            return jsonify(data)
        except Exception as e:
            print(e)
            return "ERROR:{}".format(e)
    else:
        return "no post"



if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)

